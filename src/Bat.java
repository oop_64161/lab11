public class Bat extends Animal implements Flyable{

    public Bat(String name) {
        super(name, 4);
    }

    @Override
    public void sleep() {
        System.out.println(this + " sleep.");
    }

    @Override
    public void eat() {
        System.out.println(this + " eat.");
    }

    @Override
    public String toString() {
        return "Bat (" + this.getName() + ")";
    }

    @Override
    public void takeoff() {
        System.out.println(this + " takeoff.");
    }

    @Override
    public void fly() {
        System.out.println(this + " fly.");
    }

    @Override
    public void landing() {
        System.out.println(this + " landing.");
    }

}
