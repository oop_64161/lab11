public class App {
    public static void main(String[] args) throws Exception {
        Bird bird1 = new Bird("Tweety");
        bird1.eat();
        bird1.sleep();
        bird1.takeoff();
        bird1.fly();
        bird1.landing();
        Plane boeing = new Plane("Boeing", "Rosaroi");
        boeing.takeoff();
        boeing.fly();
        boeing.landing();
        Superman clark = new Superman("Clark");
        clark.takeoff();
        clark.fly();
        clark.landing();
        clark.walk();
        clark.run();
        Human man1 = new Human("Man");
        man1.eat();
        man1.sleep();
        man1.walk();
        man1.run();
        man1.swim();
        Bat bat1 = new Bat("Huhu");
        bat1.eat();
        bat1.sleep();
        bat1.takeoff();
        bat1.fly();
        bat1.landing();
        Fish fish1 = new Fish("Tong");
        fish1.eat();
        fish1.sleep();
        fish1.swim();
        Crocodile crocodile1 = new Crocodile("Cuban");
        crocodile1.eat();
        crocodile1.sleep();
        crocodile1.swim();
        crocodile1.crawl();
        Snake snake1 = new Snake("Black Mamba");
        snake1.eat();
        snake1.sleep();
        snake1.crawl();
        Dog dog1 = new Dog("Dang");
        dog1.eat();
        dog1.sleep();
        dog1.walk();
        dog1.run();
        Cat cat1 = new Cat("Dum");
        cat1.eat();
        cat1.sleep();
        cat1.walk();
        cat1.run();
        Rat rat1 = new Rat("Ouan");
        rat1.eat();
        rat1.sleep();
        rat1.walk();
        rat1.run();

        Flyable[] flyables = { bird1, boeing, clark, bat1 };
        for (int i = 0; i < flyables.length; i++) {
            flyables[i].takeoff();
            flyables[i].fly();
            flyables[i].landing();
        }

        Walkable[] walkables = { bird1, clark, man1, dog1, cat1, rat1 };
        for (int i = 0; i < walkables.length; i++) {
            walkables[i].walk();
            walkables[i].run();
        }

        Swimable[] swimables = { man1, clark, fish1 };
        for (int i = 0; i < swimables.length; i++) {
            swimables[i].swim();
        }

        Crawlable[] crawlables = { crocodile1, snake1 };
        for (int i = 0; i < crawlables.length; i++) {
            crawlables[i].crawl();
        }
    }
}
