public abstract class Animal {
    private String name;
    private int numberOfLeg;

    public Animal(String name, int numberOfLeg) {
        this.name = name;
        this.numberOfLeg = numberOfLeg;
    }

    public String getName() {
        return this.name;
    }

    public int getNumberOfLeg() {
        return this.numberOfLeg;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setNumberOfLeg(int numberOfLeg) {
        this.numberOfLeg = numberOfLeg;
    }

    @Override
    public String toString() {
        return "Animal(" + name + ")";
    }

    public abstract void sleep();

    public abstract void eat();
}
