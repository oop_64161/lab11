public class Crocodile extends Animal implements Swimable,Crawlable{

    public Crocodile(String name) {
        super(name, 4);
    }

    @Override
    public void crawl() {
        System.out.println(this + " crawl.");
    }

    @Override
    public void swim() {
        System.out.println(this + " swim.");
    }

    @Override
    public void sleep() {
        System.out.println(this + " sleep.");
    }

    @Override
    public void eat() {
        System.out.println(this + " eat.");
    }
    
    @Override
    public String toString() {
        return "Crocodile (" + this.getName() + ")";
    }
    
}
